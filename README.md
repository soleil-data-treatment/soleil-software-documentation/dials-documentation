# Aide et ressources de DIALS pour Synchrotron SOLEIL

[<img src="https://dials.github.io/_static/dials_header.png" width="150"/>](https://dials.github.io)

## Résumé

- comme xds, indexation, intégration, merge de données (partiel, multi cristal)
- Open source

## Sources

- Code source:  https://github.com/dials
- Documentation officielle:  https://dials.github.io/documentation/index.html

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](https://dials.github.io/installation.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/131/dials) |
| [Tutoriaux officiels](https://dials.github.io/documentation/tutorials/index.html) |   |

## Installation

- Systèmes d'exploitation supportés: Linux,  python
- Installation: Difficile (très technique),  pas mal de dépendances

## Format de données

- en entrée: hdf5,  cbf,  text,  img
- en sortie: text,  listes de hkl,  mtz,  cif
- sur un disque dur
